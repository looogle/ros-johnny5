#ifndef SERVO
#define SERVO

//ROS includes
#include "ros/ros.h"
#include "johnny5/servoMsg.h"

//External includes
#include "stdio.h"
#include "sstream"
#include "boost/scoped_ptr.hpp"

//Classes declarations
class Servo
{
public:
    /**
     * Constructor.
     * \param nServ number of servo on Robot
     */
    Servo ( int nServ );

    /**
     * Get command to form line for sending on
     * ssc32. Is not ends by \r
     * \param data contains angle, velocity, time and
     * number of servo
     */
    std::string getCommand ( const johnny5::servoMsg &data );

    /**
     * Get actual postion of servo in degree
     * \return angle in degree
     */
    int getPos();

private:
    int mDegToPulse ( int angle );
    int mVelToPulse ( int vel );
    int mNServ;
    int mCurrentAngle;

    static const int M_BUFFER_SIZE;
    static const int M_SERV_RESOLUTION;
    static const float M_SPEED_RESOLUTION;
    static const int M_NEUTRAL_POS;
    static const int M_MAX_POS;
    static const int M_MIN_POS;
};

#endif // SERVO
