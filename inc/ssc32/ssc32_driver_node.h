#ifndef SSC32_DRIVER_NODE
#define SSC32_DRIVER_NODE

#include "string.h"
#include "boost/scoped_ptr.hpp"

#include "ros/ros.h"
#include "johnny5/servoMsg.h"
#include "johnny5/servoMsgArray.h"
#include "johnny5/robotState.h"

#include "robot.h"
#include "common_topics.h"

class Ssc32DriverNode {

public:
    Ssc32DriverNode(const int countOfServos, std::string usbDev);
    ~Ssc32DriverNode();

    void init();
    void run();
    void callbackFunc(const johnny5::servoMsgArray &msg);

    static const std::string NODE_NAME;

private:
    void m_msgPrint(const johnny5::servoMsgArray &msg);
    void m_saveMsg(const johnny5::servoMsgArray &msg);

    ros::NodeHandle m_nodeHandle;
    ros::Publisher m_publisher;
    ros::Subscriber m_subscriber;
    boost::scoped_ptr<johnny5::servoMsgArray> m_msg;
    boost::scoped_ptr<Robot> m_robot;

    bool m_isInited = false;
    bool m_isNewPoseDetected = false;
    
    const std::string M_USB_DEV;
    const int M_COUNT_OF_SERVOS;
    const int M_RATE_FREQUENCY = 100000;
    const int M_MESSAGE_BUFFER_SIZE = 1000;

};

#endif