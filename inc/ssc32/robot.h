#ifndef ROBOT_H
#define ROBOT_H

//ROS includes
#include <johnny5/servoMsgArray.h>
#include <johnny5/robotState.h>

//External includes
#include <sstream>
#include <stdio.h>

//Internal includes
#include <serial.h>
#include <servo.h>

//Defines
#define USB_DEV "/dev/ttyUSB0"
#define BAUDRATE 115200
#define ARE_YOU_IN_MOTION "Q\r"

//Type defines
enum RobotStates {
    ROBOT_OK = 1,
    SERIAL_NOT_CONNECTED
};

//Classes declarations
class Robot
{
public:
    /**
     * Constructor.
     * \param m_numberOfServos count of servos on Robot
     */
    Robot ( const int numberOfServos, std::string usbDev );

    /**
     * Destructor.
     */
    ~Robot();

    /**
     * Set servos in default positions.
     */
    void setDefaultPos();

    /**
     * Set positions of some servos.
     * \param msg contain messages with velocity, angle,
     * time and number of servos
     */
    void setPos ( const johnny5::servoMsgArray &msg );

    /**
     * Get actual position one of servo.
     * \param nServ number of servo
     * \return an angle in degrees
     */
    int getServoPos ( int nServ ) const;

    /**
     * Get actual positions one of all servos and state of robot (in motion or not).
     * \param msg is array for writes positions and state of robot
     */
    void getRobotState ( johnny5::robotState &msg );

    /**
     * Get number of servos.
     * \return number of servos
     */
    int getNumberOfServos() const;

    /**
     * Return robot init state.
     * \return 1 or 0 depend robot state.
     */
    int8_t isRobotOk() const;

    /**
     * Return answer-string from SSC32.
     * \return ...
     */
    void getAnswer ( std::string question ) const;

private:
    const int M_NUMBER_OF_SERVOS;
    const std::string M_USB_DEV;
    
    RobotStates mCurrentRobotState;
    Serial *mSerial;
    std::vector<Servo*> mServos;

    void mInitSerial();

    void mCatchError ( RobotStates error, boost::system::system_error& e );
};

#endif // ROBOT_H

