//
// Created by dim on 21.08.15.
//

#ifndef JOHNNY_5_WS_COMMON_DEFINES_H
#define JOHNNY_5_WS_COMMON_DEFINES_H

//Defines
#define PI 3.14159265

//Macros
#define MS_TO_HZ(x) 1000/(float)x
#define ABS(x) (x < 0 ? -x : x)
#define TO_DEG(x) x * 180.0 / PI

#endif //JOHNNY_5_WS_COMMON_DEFINES_H
