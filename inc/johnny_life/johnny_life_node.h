#ifndef JOHNNY_LIFE_NODE_H
#define JOHNNY_LIFE_NODE_H

//ROS includes
#include "ros/ros.h"

//External includes
//--

//Internal includes
#include "commons.h"
#include "positions.h"
#include "johnny_life.h"

//Local includes
//--

//Defines
#define NODE_NAME "johnny_life"
#define MIN_DISTANCE 0.65
#define STANDARD_RATE_FREQUENCY 1 //Hz

#endif // JOHNNY_LIFE_NODE_H
