#ifndef POSITIONS_H
#define POSITIONS_H

//ROS includes
#include "johnny5/servoMsgArray.h"

//External includes
#include "vector"

//Internal includes
//--

//Local includes
//--

//Defines
#define COUNT_OF_SERVOS 16
#define STANDARD_TIME 1000
#define STANDARD_VELOCITY 70

//Type defines
enum Position {
    DEFAULT_POS,
    GREETING_POS,
    GIVING_POS
};

//Classes declarations
struct Positions {
private:
    static const uint16_t mDefaultPosLength;
    static const uint16_t mGreetingPosLength;
    static const uint16_t mGivingPosLength;

public:
    class Initializer
    {
    public:
        Initializer();
    };

    friend class Initializer;

    Positions();

    static std::vector<johnny5::servoMsgArray> defaultPos;
    static std::vector<johnny5::servoMsgArray> greetingPos;
    static std::vector<johnny5::servoMsgArray> givingPos;

    static johnny5::servoMsgArray getGivingPos ( int positionNo, int *posArray, int arrayLength );

    static void changePosition ( Position pos, int16_t nPos, int16_t nServo, int16_t value );

    //init all static fields
    static Initializer initializer;
};

#endif // POSITIONS_H
