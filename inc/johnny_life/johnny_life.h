#ifndef JOHNNY_LIFE_H
#define JOHNNY_LIFE_H

//ROS includes
#include "ros/ros.h"
//messages
#include "johnny5/servoMsgArray.h"
#include "johnny5/servoMsg.h"
#include "johnny5/robotState.h"
#include "ppl_detection/Tracker.h"
#include "ppl_detection/Human.h"

//External includes
#include "cmath"
#include "vector"

//Internal includes
#include "commons.h"
#include "positions.h"

//Type defines
enum Timer {
    GREETING_TIMER,
    ROTATION_TIMER,
    ANOTHER_TIMER
};

enum HumansState {
    NO_HUMAN,
    HUMAN_DETECTED
};

//Classes declarations
struct HumanCoords {
    float x;
    float y;
    float z;
    float distance;
    int32_t humNo;
};

class LifeNode
{
private:
    bool mInMotion;
    std::vector<HumanCoords> visibleHumans;
    int mCurrentHumanNo;
    int mRotationAngle;
    ros::NodeHandle mNodeHandler;
    ros::Publisher mPublisher;
    ros::Subscriber mRobotStateSubscriber;
    ros::Subscriber mPeoplePoseSubscriber;
    ros::Timer mGreetingTimer;
    ros::Timer mRotationTimer;
    HumansState mHumansState;

    void mSubRobotStateCallback ( const johnny5::robotState &msg );

    void mSubPeoplePoseCallback ( const ppl_detection::Tracker &msg );

    void mGreetingTimerCallback ( const ros::TimerEvent &e );

    void mRotationTimerCallback ( const ros::TimerEvent &e );

    int mFilterHumansByDistance() const;

    void mSetHumansState ( HumansState humansState );

    static const int ROTATION_SPEED;
    static const int ROTATION_TIME;
    static const int ROTATION_TIMER_DURATION;
    static const int GREETING_TIMER_DURATION;
    static const int SIZE_MESSAGE_BUFFER;

public:
    LifeNode();

    ~LifeNode();

    void publish ( const johnny5::servoMsgArray &msg );

    void waitWhileMoving ( const int rateFrequency = 10000);

    void timerInit ( const Timer t );

    void timerKill ( const Timer t );

    void timerStart ( const Timer t );

    bool isInMotion() const;

    int getNumSubscribers() const;

    void updateHumansPosition ( HumanCoords &human );

    int getCurrentHumanNo() const;

    void setRotationAngle ( int angle );

    bool isHumanDetected() const;

    static const int SERVO_ANGLE_ERROR;
};

#endif //JOHNNY_LIFE_H
