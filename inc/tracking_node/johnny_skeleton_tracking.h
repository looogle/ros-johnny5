#ifndef JOHNNY_SKELETON_TRACKING
#define JOHNNY_SKELETON_TRACKING

//ROS includes
#include "ros/ros.h"

//Internal includes
#include "common_topics.h"

#define NODE_NAME "tracking_node"
#define SIZE_MESSAGE_BUFFER 1024

#endif // JOHNNY_SKELETON_TRACKING

