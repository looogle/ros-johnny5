#ifndef RAW_MODEL
#define RAW_MODEL

//ROS includes
#include "ros/ros.h"
#include "tf/transform_listener.h"

//External includes

//Internal includes

//Local includes

//#define TRANSFORM_SOURCE /openni_depth_frame
//#define HEAD_TRANSFORM head_1
//#define HEAD_TRANSFORM neck_1
//#define tf::StampedTransform torso_1
//#define tf::StampedTransform left_shoulder_1
//#define tf::StampedTransform left_elbow_1
//#define tf::StampedTransform left_hand_1
//#define tf::StampedTransform right_shoulder_1
//#define tf::StampedTransform right_elbow_1
//#define tf::StampedTransform right_hand_1
//#define tf::StampedTransform left_hip_1
//#define tf::StampedTransform left_knee_1
//#define tf::StampedTransform left_foot_1
//#define tf::StampedTransform right_hip_1
//#define tf::StampedTransform right_knee_1
//#define tf::StampedTransform right_foot_1

//Defined types and structs
struct point3D {
    float x;
    float y;
    float z;
};

struct bodyCoords {
    point3D head;
    point3D neck;
    point3D torso;
    point3D left_shoulder;
    point3D left_elbow;
    point3D left_hand;
    point3D right_shoulder;
    point3D right_elbow;
    point3D right_hand;
    point3D left_hip;
    point3D left_knee;
    point3D left_foot;
    point3D right_hip;
    point3D right_knee;
    point3D right_foot;
};

struct bodyTranforms {
    tf::StampedTransform head;
    tf::StampedTransform neck;
    tf::StampedTransform torso;
    tf::StampedTransform left_shoulder;
    tf::StampedTransform left_elbow;
    tf::StampedTransform left_hand;
    tf::StampedTransform right_shoulder;
    tf::StampedTransform right_elbow;
    tf::StampedTransform right_hand;
    tf::StampedTransform left_hip;
    tf::StampedTransform left_knee;
    tf::StampedTransform left_foot;
    tf::StampedTransform right_hip;
    tf::StampedTransform right_knee;
    tf::StampedTransform right_foot;
};

// Class prototypes
class rawModel
{
public:
    rawModel();
    ~rawModel();
    getBodyCoords();
    coordsUpdater();
private:
    tf::TransformListener mListener;
    bodyCoords body;

}

#endif // RAW_MODEL

