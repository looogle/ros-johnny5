#ifndef BLOCKING_READER_H
#define BLOCKING_READER_H

//
// blocking_reader - a class that provides basic support for
// blocking & time-outable single character reads from
// boost::asio::serial_port.
//
// use like this:
//
//  blocking_reader reader(port, 500);
//
//  char c;
//
//  if (!reader.read_char(c))
//      return false;
//
// Kevin Godden, www.ridgesolutions.ie
//
#include <boost/asio.hpp>
#include <boost/bind.hpp>

class BlockingReader
{
public:
    BlockingReader ( boost::asio::serial_port& port, size_t timeout );
    ~BlockingReader();

    bool read_char ( char& val );

private:
    boost::asio::serial_port& port;
    size_t timeout;
    char c;
    boost::asio::deadline_timer timer;
    bool read_error;

    void read_complete ( const boost::system::error_code& error,
                         size_t bytes_transferred );
    void time_out ( const boost::system::error_code& error );
};

#endif //BLOCKING_READER_H
