//Local includes
#include "positions.h"

const uint16_t Positions::mDefaultPosLength = 1;
const uint16_t Positions::mGreetingPosLength = 2;
const uint16_t Positions::mGivingPosLength = 6;

std::vector<johnny5::servoMsgArray> Positions::defaultPos(mDefaultPosLength);
std::vector<johnny5::servoMsgArray> Positions::greetingPos(mGreetingPosLength);
std::vector<johnny5::servoMsgArray> Positions::givingPos(mGivingPosLength);

Positions::Initializer Positions::initializer;

Positions::Positions() {
}

Positions::Initializer::Initializer() {
    johnny5::servoMsg data;

    /* Start: default position */
    int defaultPosition[] = {
        -7,     //0 - Torso
        -37,    //1 - Bottom back
        40,     //2 - Top back
        0,      //3 - Head
        -40,    //4 - Right gripper
        80,     //5 - Right elbow
        -5,     //6 - Right forearm
        -75,    //7 - Right shoulder
        90,     //8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     //11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = defaultPosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        defaultPos[0].servoData.push_back(data);
    }

    /* End */

    /* Start: greeting position */
    int greetingPosition[] = {
        -7,     //0 - Torso
        -37,    //1 - Bottom back
        40,     //2 - Top back
        0,      //3 - Head
        -15,    //4 - Right gripper
        45,     //5 - Right elbow
        80,     //6 - Right forearm
        0,    	//7 - Right shoulder
        -90,    //8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     //11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 1; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;

        if (i == 7) {
            data.time = 1000;
            data.vel = 90;
            data.angle = -90;
            greetingPos[0].servoData.push_back(data);
            data.angle = -40;
            greetingPos[1].servoData.push_back(data);
        }
        else {
            data.time = STANDARD_TIME;
            data.vel = STANDARD_VELOCITY;
            data.angle = greetingPosition[i];
            greetingPos[0].servoData.push_back(data);
            greetingPos[1].servoData.push_back(data);
        }
    }

    /* End */

    /*Start: giving position */
    /* rotation */
    int rotationPosition[] = {
        -90,    //0 - Torso
        -37,    //1 - Bottom back
        40,     //2 - Top back
        0,     //3 - Head
        45,     //4 - Right gripper
        65,     //5 - Right elbow
        -90,    //6 - Right forearm
        -75,    //7 - Right shoulder
        0,    	//8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     	//11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = rotationPosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        givingPos[0].servoData.push_back(data);
    }

    /* incline */
    int inclinePosition[] = { 
        -90,    //0 - Torso
        0,    //1 - Bottom back
        20,     //2 - Top back
        45,     //3 - Head
        45,     //4 - Right gripper
        65,     //5 - Right elbow
        -90,    //6 - Right forearm
        -75,    //7 - Right shoulder
        0,    	//8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     	//11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = inclinePosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        givingPos[1].servoData.push_back(data);
    }

    /* catch */
    int catchPosition[] = { 
        -90,    //0 - Torso
        0,    //1 - Bottom back
        20,     //2 - Top back
        45,     //3 - Head
        -90,     //4 - Right gripper
        65,     //5 - Right elbow
        -90,    //6 - Right forearm
        -75,    //7 - Right shoulder
        0,    	//8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     	//11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = catchPosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        givingPos[2].servoData.push_back(data);
    }

    /* back */
    int backPosition[] = {
        -90,    //0 - Torso
        -37,    //1 - Bottom back
        40,     //2 - Top back
        45,     //3 - Head
        -90,     //4 - Right gripper
        65,     //5 - Right elbow
        -90,    //6 - Right forearm
        -75,    //7 - Right shoulder
        0,    	//8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     	//11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = backPosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        givingPos[3].servoData.push_back(data);
    }

    /* backtation */
    int backtationPosition[] = {
        -7,    //0 - Torso
        -37,    //1 - Bottom back
        40,     //2 - Top back
        0,     //3 - Head
        -90,     //4 - Right gripper
        20,     //5 - Right elbow
        -90,    //6 - Right forearm
        -75,    //7 - Right shoulder
        0,    	//8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     	//11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = backtationPosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        givingPos[4].servoData.push_back(data);
    }

    /* give */
    int givePosition[] = { 
        -7,    //0 - Torso
        -37,    //1 - Bottom back
        40,     //2 - Top back
        0,     //3 - Head
        -20,     //4 - Right gripper
        20,     //5 - Right elbow
        -90,    //6 - Right forearm
        -75,    //7 - Right shoulder
        10,    	//8 - Right arm
        40,     //9 - Left gripper
        -85,    //10 - Left elbow
        5,     	//11 - Left forearm
        90,     //12 - Left shoulder
        -85,    //13 - Left arm
        0,      //14 - Not used
        0       //15 - Not used
    };

    for (int i = 0; i < COUNT_OF_SERVOS; ++i) {
        data.nServ = i;
        data.angle = givePosition[i];
        data.time = STANDARD_TIME;
        data.vel = STANDARD_VELOCITY;
        givingPos[5].servoData.push_back(data);
    }

    /* End */
}

void Positions::changePosition(Position pos, int16_t nPos, int16_t nServo, int16_t value) {
    johnny5::servoMsgArray *msg;
    johnny5::servoMsg *data;

    switch (pos) {
        case DEFAULT_POS:
            msg = &defaultPos[nPos];
            data = &msg->servoData[nServo];
            break;

        case GREETING_POS:
            msg = &greetingPos[nPos];
            data = &msg->servoData[nServo];
            break;

        case GIVING_POS:
            msg = &givingPos[nPos];
            data = &msg->servoData[nServo];
            break;
    }

    data->angle = value;
}
