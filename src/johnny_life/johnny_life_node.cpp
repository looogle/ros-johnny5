//Local includes
#include "johnny_life_node.h"

int main(int argc, char *argv[]) {
    ros::init(argc, argv, NODE_NAME);
    LifeNode lifeNode;

    HumanCoords humanCoords;

    ros::Rate poll_rate(100);

    while (lifeNode.getNumSubscribers() == 0) {
        poll_rate.sleep();
    }

    lifeNode.publish(Positions::defaultPos[0]);
    lifeNode.waitWhileMoving(STANDARD_RATE_FREQUENCY);

    lifeNode.timerInit(GREETING_TIMER);

    int angle = 0;
    float distance = 0;
    int oldHumNo = -1;

    ROS_INFO_ONCE("Greeting timer started.\n");
    lifeNode.timerInit(ROTATION_TIMER);

    ros::Rate rate(MS_TO_HZ(50));

    while (ros::ok()) {
        ROS_INFO_ONCE("Human detect started.");
        ROS_INFO_ONCE("Waiting human detect...\n");

        lifeNode.updateHumansPosition(humanCoords);

        if (lifeNode.isHumanDetected()) {
            angle = (int)(TO_DEG(atan(humanCoords.x / humanCoords.z)));
            distance = humanCoords.distance;

            lifeNode.setRotationAngle(angle);

            ROS_INFO_STREAM("#" << humanCoords.humNo <<
                            " angle" << " -> " << angle <<
                            "; distance -> " << distance);

            if (distance < MIN_DISTANCE && (lifeNode.getCurrentHumanNo() != oldHumNo)) {
                lifeNode.timerKill(GREETING_TIMER);
                lifeNode.timerKill(ROTATION_TIMER);

                for (int pos = 0; pos < Positions::givingPos.size(); ++pos) {
                    if (pos == 5 || pos == 4) {
                        if (pos == 5) {
                            ros::Duration(1).sleep();
                        }

                        Positions::changePosition(GIVING_POS, pos, 0, angle + LifeNode::SERVO_ANGLE_ERROR);
                        lifeNode.publish(Positions::givingPos[pos]);
                    }
                    else {
                        lifeNode.publish(Positions::givingPos[pos]);
                    }

                    lifeNode.waitWhileMoving(STANDARD_RATE_FREQUENCY);
                }

                rate.sleep();

                lifeNode.timerStart(GREETING_TIMER);
                lifeNode.timerStart(ROTATION_TIMER);
                oldHumNo = lifeNode.getCurrentHumanNo();
            }
        }
        else {
            ROS_WARN("No humans detected!");
        }

        ros::spinOnce();
        rate.sleep();
    }

    ros::shutdown();
}
