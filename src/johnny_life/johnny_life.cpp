//Local includes
#include "johnny_life.h"

const int LifeNode::ROTATION_SPEED = 50;
const int LifeNode::ROTATION_TIME = 50;
const int LifeNode::ROTATION_TIMER_DURATION = 0.3; //s
const int LifeNode::GREETING_TIMER_DURATION = 1;   //s
const int LifeNode::SERVO_ANGLE_ERROR = -7;
const int LifeNode::SIZE_MESSAGE_BUFFER = 10000;

LifeNode::LifeNode() :
    mInMotion(false),
    mCurrentHumanNo(0),
    mRotationAngle(0),
    mPublisher(
        mNodeHandler.advertise<johnny5::servoMsgArray>(
            TOPIC_SERVO_CONTROL,
            SIZE_MESSAGE_BUFFER
        )
    ),
    mRobotStateSubscriber(
        mNodeHandler.subscribe(
            TOPIC_ROBOT_STATE,
            SIZE_MESSAGE_BUFFER,
            &LifeNode::mSubRobotStateCallback,
            this
        )
    ),
    mPeoplePoseSubscriber(
        mNodeHandler.subscribe(
            TOPIC_HUMAN_POS,
            SIZE_MESSAGE_BUFFER,
            &LifeNode::mSubPeoplePoseCallback,
            this)
    ) 
{

}

LifeNode::~LifeNode() {
    mPublisher.shutdown();
    mRobotStateSubscriber.shutdown();
    mPeoplePoseSubscriber.shutdown();
}

/****** SUBSCRIBERS CALLBACKS ******/

void LifeNode::mSubRobotStateCallback(const johnny5::robotState &msg) {
    mInMotion = (msg.inMotion == 1);
}

void LifeNode::mSubPeoplePoseCallback(const ppl_detection::Tracker &msg) {
    visibleHumans.clear();

    if (msg.human.size() > 0) {
        HumanCoords humanCoords;

        for (int i = 0; i < msg.human.size(); ++i) {
            humanCoords.x = msg.human[i].pos_x;
            humanCoords.y = msg.human[i].pos_y;
            humanCoords.z = msg.human[i].pos_z;
            humanCoords.distance = msg.human[i].distance;
            humanCoords.humNo = msg.human[i].tracking_no;
            visibleHumans.push_back(humanCoords);
        }

        mSetHumansState(HUMAN_DETECTED);
    }
    else {
        mSetHumansState(NO_HUMAN);
    }
}

/****** TIMERS CALLBACKS ******/

void LifeNode::mGreetingTimerCallback(const ros::TimerEvent &e) {
    static bool greetingPose = true;

    greetingPose = !greetingPose;

    this->publish(Positions::greetingPos[greetingPose]);
}

void LifeNode::mRotationTimerCallback(const ros::TimerEvent &e) {
    static johnny5::servoMsgArray servoControlMsg;
    static johnny5::servoMsg servoData;

    servoData.nServ = 0;
    servoData.time = ROTATION_TIME;
    servoData.vel = ROTATION_SPEED;
    servoData.angle = mRotationAngle + SERVO_ANGLE_ERROR;

    servoControlMsg.servoData.clear();
    servoControlMsg.servoData.push_back(servoData);

    this->publish(servoControlMsg);
}

/****** OTHER STUFF ******/

void LifeNode::publish(const johnny5::servoMsgArray &msg) {
    waitWhileMoving();
    mPublisher.publish(msg);
    mInMotion = true;
}

int LifeNode::getNumSubscribers() const {
    return mPublisher.getNumSubscribers();
}

void LifeNode::timerInit(const Timer t) {
    switch (t) {
        case GREETING_TIMER:
            mGreetingTimer = mNodeHandler.createTimer(
                                 ros::Duration(GREETING_TIMER_DURATION),
                                 &LifeNode::mGreetingTimerCallback, this);
            break;

        case ROTATION_TIMER:
            mRotationTimer = mNodeHandler.createTimer(
                                 ros::Duration(ROTATION_TIMER_DURATION),
                                 &LifeNode::mRotationTimerCallback, this);
            break;

        case ANOTHER_TIMER:
            //another time init
            break;

        default:
            break;
    }
}

void LifeNode::timerStart(const Timer t) {
    switch (t) {
        case GREETING_TIMER:
            mGreetingTimer.start();
            break;

        case ROTATION_TIMER:
            mRotationTimer.start();
            break;

        case ANOTHER_TIMER:
            //another time start
            break;

        default:
            break;
    }
}

void LifeNode::timerKill(const Timer t) {
    switch (t) {
        case GREETING_TIMER:
            mGreetingTimer.stop();
            break;

        case ROTATION_TIMER:
            mRotationTimer.stop();
            break;

        case ANOTHER_TIMER:
            //another time stop
            break;

        default:
            break;
    }
}

bool LifeNode::isInMotion() const {
    return mInMotion;
}

int LifeNode::mFilterHumansByDistance() const {
    float minDistance = visibleHumans[0].distance;
    int closestHumanId = 0;

    for (int i = 0; i < visibleHumans.size(); ++i) {
        if (visibleHumans[i].distance < minDistance) {
            closestHumanId = i;
            minDistance = visibleHumans[i].distance;
        }
    }

    return closestHumanId;
}

void LifeNode::updateHumansPosition(HumanCoords &human) {
    static int closestHumanId;

    if (isHumanDetected()) {
        closestHumanId = mFilterHumansByDistance();

        mCurrentHumanNo = visibleHumans[closestHumanId].humNo;

        human.humNo = visibleHumans[closestHumanId].humNo;
        human.x = visibleHumans[closestHumanId].x;
        human.y = visibleHumans[closestHumanId].y;
        human.z = visibleHumans[closestHumanId].z;
        human.distance = visibleHumans[closestHumanId].distance;

        mCurrentHumanNo = closestHumanId;
    }
}

void LifeNode::waitWhileMoving(const int rateFrequency) {
    ros::Rate rate(rateFrequency);
    rate.sleep();

    while (isInMotion()) {
        rate.sleep();
        ros::spinOnce();
    }
}

int LifeNode::getCurrentHumanNo() const {
    return mCurrentHumanNo;
}

void LifeNode::setRotationAngle(int angle) {
    mRotationAngle = angle;
}

bool LifeNode::isHumanDetected() const {
    return mHumansState == HUMAN_DETECTED;
}

void LifeNode::mSetHumansState(HumansState humansState) {
    mHumansState = humansState;
}
