#include "signal.h"

#include "ros/ros.h"
#include "johnny5/servoMsg.h"
#include "johnny5/servoMsgArray.h"
#include "johnny5/robotState.h"

#include "robot.h"
#include "common_topics.h"

#include "ssc32_driver_node.h"

const std::string Ssc32DriverNode::NODE_NAME = "ssc32_driver";

Ssc32DriverNode::Ssc32DriverNode(const int countOfServos, 
                                 std::string usbDev) : 
        M_COUNT_OF_SERVOS(countOfServos),
        M_USB_DEV(usbDev)
{
    init();
}

Ssc32DriverNode::~Ssc32DriverNode() {

}

void Ssc32DriverNode::init() {
    if (m_isInited) {
        ROS_ERROR_ONCE("Object Ssc32DriverNode is already initialized!");
    }
    else {
        if (ros::ok()){
            m_publisher = m_nodeHandle.advertise<johnny5::robotState>(
                    TOPIC_ROBOT_STATE, 
                    M_MESSAGE_BUFFER_SIZE
                );
            m_subscriber = m_nodeHandle.subscribe(
                    TOPIC_SERVO_CONTROL, 
                    M_MESSAGE_BUFFER_SIZE, 
                    &Ssc32DriverNode::callbackFunc, 
                    this
                );
            m_robot.reset(new Robot(M_COUNT_OF_SERVOS, M_USB_DEV));
            ROS_INFO_STREAM("Driver started on " << M_USB_DEV);
        }
        else {
            ROS_ERROR_ONCE("ROS not initialized yet!");
            exit(1);
        }
    }
}

void Ssc32DriverNode::m_msgPrint(const johnny5::servoMsgArray &msg) {
    for (int i = 0; i < msg.servoData.size(); ++i) {
        const johnny5::servoMsg &data = msg.servoData[i];

        if (data.time > 0 && data.vel > 0) {
            ROS_INFO_STREAM("#" << data.nServ << "; " <<
                            "angle=" << data.angle << "; " <<
                            "time=" << data.time << "; " <<
                            "velocity=" << data.vel);
        }
        else {
            ROS_INFO_STREAM("#" << data.nServ << "; " <<
                            "level=" << data.angle << "; ");
        }
    }
    ROS_INFO_STREAM("########################################");
}

void Ssc32DriverNode::callbackFunc(const johnny5::servoMsgArray &msg) {
    m_isNewPoseDetected = true;

    /* Save msg in member m_msg */
    m_saveMsg(msg);

    /* Print robot new position */
    m_msgPrint(msg);
}

void Ssc32DriverNode::run() {
    ros::Rate rate(M_RATE_FREQUENCY);
   

    while (ros::ok()) {
        if (m_robot->isRobotOk()) {

            ROS_INFO_ONCE("Driver has started.");
            ROS_INFO_ONCE("Waiting commands...");

            /* Set robot position messages */
            if (m_isNewPoseDetected) {
                m_robot->setPos(*m_msg);
                m_isNewPoseDetected = false;
            }

            /* Publish robot current position */
            johnny5::robotState msg;
            m_robot->getRobotState(msg);

            m_publisher.publish(msg);

            rate.sleep();
            ros::spinOnce();
        }
        else {
            ROS_ERROR_ONCE("Robot initialization failed.");
            ROS_ERROR_ONCE("Check /dev/ttyUSB0 connect");
            break;
        }
    }
}

void Ssc32DriverNode::m_saveMsg(const johnny5::servoMsgArray &msg) {
    m_msg.reset(new johnny5::servoMsgArray(msg));
}
