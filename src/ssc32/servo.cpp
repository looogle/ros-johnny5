//Local includes
#include <servo.h>

const int Servo::M_BUFFER_SIZE = 10;
const int Servo::M_SERV_RESOLUTION = 10;
const float Servo::M_SPEED_RESOLUTION = 11.1;
const int Servo::M_NEUTRAL_POS = 1500;
const int Servo::M_MAX_POS = 2400;
const int Servo::M_MIN_POS = 600;

Servo::Servo(int nServ) :
    mNServ(nServ)
{
    //mPositionBuffer.resize(M_BUFFER_SIZE);
    // First state can not be defined
    mCurrentAngle = -1;
}

int Servo::getPos() {
    return mCurrentAngle;
}

int Servo::mDegToPulse(int angle) {
    int retAngle = M_NEUTRAL_POS + angle * M_SERV_RESOLUTION;
    return retAngle;
}

int Servo::mVelToPulse(int vel) {
    return vel * M_SPEED_RESOLUTION;
}

std::string Servo::getCommand(const johnny5::servoMsg &data) {
    std::stringstream str;

    //if pulse
    if (data.vel > 0 && data.time > 0) {
        str << "#" << data.nServ << " P" << mDegToPulse(data.angle) <<
            " S" << mVelToPulse(data.vel) << " T" << data.time;
        mCurrentAngle = data.angle;
    }
    //if discrete value
    else {
        str << "#" << data.nServ;

        if (data.angle == 1) {
            str << "H";
        }
        else {
            str << "L";
        }
    }

    return str.str();
}
