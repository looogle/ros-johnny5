//Local includes
#include "serial.h"


Serial::Serial(std::string port, unsigned int baud_rate) 
        : m_serialPort(m_io) 
{
    m_io.run();
    open(port, baud_rate);
}

Serial::~Serial() {
    m_serialPort.close();

}

void Serial::open(const std::string& portname, int baudrate) {
    m_serialPort.open(portname.c_str());
    m_serialPort.set_option(boost::asio::serial_port_base::baud_rate(baudrate));
}

void Serial::close() {
    m_serialPort.close();
}

size_t Serial::receive(void* data, size_t length) {
    return m_serialPort.read_some(boost::asio::buffer(data, length));
}

size_t Serial::send(void* data, size_t length) {
    return m_serialPort.write_some(boost::asio::buffer(data, length));
}

std::string Serial::readString() {
    char data[256];
    receive(&data, 256);
    std::string s = data;
    return data;
}

char Serial::readChar() {
    char c;
    if (receive(&c, 1)) {
        return c;
    }
    return 0;
}

void Serial::writeString(std::string s) {
    send((void*)s.c_str(), s.size());
}
