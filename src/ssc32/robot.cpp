//Local includes
#include <robot.h>

Robot::Robot(const int numberOfServos, 
             const std::string usbDev):
    M_NUMBER_OF_SERVOS(numberOfServos),
    M_USB_DEV(usbDev)
{
    mCurrentRobotState = ROBOT_OK;

    mInitSerial();

    mServos.resize(M_NUMBER_OF_SERVOS);
    for (int i = 0; i < M_NUMBER_OF_SERVOS; ++i) {
        mServos[i] = new Servo(i);
    }
}

Robot::~Robot() {
    //Delete mServos
    for (int i = 0; i < M_NUMBER_OF_SERVOS; ++i) {
        delete mServos[i];
    }
    
    //Delete mSerial
    if (mSerial) {
        delete mSerial;
    }

    mSerial = NULL;
}

void Robot::setPos(const johnny5::servoMsgArray &msg) {
    std::stringstream str;

    for (int i = 0; i < msg.servoData.size(); ++i) {
        const johnny5::servoMsg &data = msg.servoData[i];
        if (data.nServ >= M_NUMBER_OF_SERVOS) {
            ROS_ERROR_STREAM("Can't set pose of servo " << data.nServ <<
                             " because it's more then coout of servos!");
            return;
        }
        str << mServos[data.nServ]->getCommand(data) << " ";
    }

    str << "\r";

    try {
        mSerial->writeString(str.str());
    }
    catch (boost::system::system_error& e) {
        mCatchError(SERIAL_NOT_CONNECTED, e);
    }
}

int Robot::getServoPos(int nServ) const {
    return mServos[nServ]->getPos();
}

void Robot::getRobotState(johnny5::robotState &msg) {
    char c;

    try {
        mSerial->writeString(ARE_YOU_IN_MOTION);
        c = mSerial->readChar();
    }
    catch (boost::system::system_error& e) {
        mCatchError(SERIAL_NOT_CONNECTED, e);
    }

    if (c == '+') {
        msg.inMotion = 1;
    }
    else {
        msg.inMotion = 0;
    }

    for (int iServ = 0; iServ < M_NUMBER_OF_SERVOS; ++iServ) {
        msg.positions.push_back(getServoPos(iServ));
    }
}

int Robot::getNumberOfServos() const {
    return M_NUMBER_OF_SERVOS;
}

void Robot::getAnswer(std::string question) const {
    std::string answer;

    mSerial->writeString(question);
    answer = mSerial->readString();

    ROS_INFO_STREAM(answer.c_str());
}

int8_t Robot::isRobotOk() const {
    if (mCurrentRobotState == ROBOT_OK) {
        return 1;
    }

    return 0;
}

void Robot::mCatchError(RobotStates err, boost::system::system_error& e) {
    ROS_ERROR_STREAM("Boost debug print: " << e.what());

    switch (err) {
        case SERIAL_NOT_CONNECTED:
            mCurrentRobotState = SERIAL_NOT_CONNECTED;
            ROS_ERROR_STREAM("Suggestion: " << M_USB_DEV << " was not plugged in.");
            break;
    }
}


void Robot::mInitSerial() {
    try {
        mSerial = new Serial(M_USB_DEV, BAUDRATE);
    }
    catch (boost::system::system_error& e) {
        mCatchError(SERIAL_NOT_CONNECTED, e);
    }
}
