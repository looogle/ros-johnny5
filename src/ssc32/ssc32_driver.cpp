#include "ros/ros.h"

#include "signal.h"
#include "string"

#include "ssc32_driver_node.h"

void sigInterruptHandler(int sig) {
    ROS_WARN_STREAM("Driver was interrupted by user!");
    ros::shutdown();
}

int main(int argc, char *argv[]) {
    ros::init(argc, argv, Ssc32DriverNode::NODE_NAME, ros::init_options::NoSigintHandler);

    int countOfServos = 16;
    std::string devUsb = "/dev/ttyUSB0";

    if (argc > 1) {
        for (int i = 1; i < argc; i+=2) {
            std::string arg = argv[i];
            if (arg == "-d"){
                devUsb = argv[i+1];
            }
            else if (arg == "-c"){
                std::istringstream iss(argv[i+1]);
                iss >> countOfServos;
            }
        }
    }

    /* Start driver */
    Ssc32DriverNode ssc32Driver(countOfServos, devUsb);
    ssc32Driver.run();

    /* Catch Ctr+C */
    signal(SIGINT, sigInterruptHandler);

    ros::shutdown();
    return 0;
}
