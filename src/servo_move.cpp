//Global includes
#include <stdio.h>
#include <sstream>

//ROS includes
#include <ros/ros.h>

//Local includes
#include "common_topics.h"

//Messages
#include <johnny5/servoMsgArray.h>
#include <johnny5/servoMsg.h>

//Defines
#define SIZE_MESSAGE_BUFFER 1000
#define STRINGSTREAM_CLEAR(x) x.str(std::string()); x.clear()

int main(int argc, char **argv) {
    ros::init(argc, argv, "servo_move");

    ros::NodeHandle nodeHandler;
    ros::Publisher publisher = nodeHandler.advertise<johnny5::servoMsgArray>(TOPIC_SERVO_CONTROL,
                               SIZE_MESSAGE_BUFFER);

    johnny5::servoMsgArray servoControlMsg;
    johnny5::servoMsg servoData;
    ros::Rate rate(2);

    std::stringstream str;

    int16_t countOfParams = 0;

    //Get count of params
    str << argv[1];
    str >> countOfParams;
    STRINGSTREAM_CLEAR(str);

    if (countOfParams < 2) {
        ROS_ERROR_STREAM("Enter the number of parameters!");
        goto end;
    }
    else if (countOfParams > 4) {
        ROS_ERROR_STREAM("Too many parameters!");
        goto end;
    }

    servoControlMsg.servoData.clear();

    for (int i = 2; i < argc; i += countOfParams) {

        if ((argc - 2) % countOfParams != 0) {
            ROS_ERROR_STREAM("Not all parameters was entered!");
            goto end;
        }

        //Get No. of servo and pulse
        str << argv[i] << " " << argv[i + 1];
        str >> servoData.nServ >> servoData.angle;
        STRINGSTREAM_CLEAR(str);

        //Default values of time and velocity
        servoData.time = 50;
        servoData.vel = 100;

        //Get time and velocity if exist
        if (countOfParams >= 3) {
            str << argv[i + 2];
            str >> servoData.time;
            STRINGSTREAM_CLEAR(str);
        }

        if (countOfParams == 4) {
            str << argv[i + 3];
            str >> servoData.vel;
            STRINGSTREAM_CLEAR(str);
        }

        if (servoData.vel > 0 && servoData.time > 0) {
            ROS_INFO_STREAM("On servo #" << servoData.nServ << " angle " << servoData.angle << " was sended.");
        }
        else {
            ROS_INFO_STREAM("On servo #" << servoData.nServ << " level " << servoData.angle << " was sended.");
        }

        STRINGSTREAM_CLEAR(str);
        servoControlMsg.servoData.push_back(servoData);
    }

    while (ros::ok()) {
        ros::Duration(1).sleep();
        publisher.publish(servoControlMsg);
        rate.sleep();
        ros::spinOnce();
        break;
    }

end:
    ros::shutdown();
    return 0;
}
