#include <ros/ros.h>
#include "common_topics.h"
#include <tf/transform_listener.h>
#include <math.h>

#include <johnny5/servoMsgArray.h>
#include <johnny5/servoMsg.h>

#define SIZE_MESSAGE_BUFFER 1000
#define TO_DEG(x) x * 180.0 / M_PI
#define TO_RAD(x) x * M_PI / 180.0

double mRound(double x) {
    double intPart, fractPart;
    fractPart = modf(x, &intPart);
    return intPart + round(fractPart * 100) / 100;
}

struct coords {
    double x;
    double y;
    double z;
};

int main(int argc, char **argv) {
    ros::init(argc, argv, "tf_listener");
    ros::NodeHandle node;

    tf::TransformListener listener;
    ros::Rate rate(10.0);

    tf::StampedTransform torsoTransform, neckTransform;
    tf::StampedTransform leftHandTransform, leftElbowTransform, leftShoulderTransform;
    tf::StampedTransform rightHandTransform, rightElbowTransform, rightShoulderTransform;
    coords leftHand, leftElbow, leftShoulder, torso, neck;
    coords rightHand, rightElbow, rightShoulder;

    int angle13, angle12, angle11, angle10;
    int angle8, angle7, angle6, angle5;
    int angle0, angle2;
    int prevAngle10, prevAngle11, prevAngle12, prevAngle13;

    double deltaX = 0;
    double deltaY = 0;
    double deltaZ = 0;

    ros::NodeHandle mNodeHandler;
    ros::Publisher publisher = mNodeHandler.advertise<johnny5::servoMsgArray>(TOPIC_SERVO_CONTROL,
                               SIZE_MESSAGE_BUFFER);

    johnny5::servoMsgArray servoControlMsg;
    johnny5::servoMsg servoData;

    angle12 = 90;
    angle13 = -90;

    while (node.ok()) {
        try {
            //Получаем трансформации
            listener.lookupTransform("/openni_depth_frame", "/left_hand_1", ros::Time(0), leftHandTransform);
            listener.lookupTransform("/openni_depth_frame", "/left_elbow_1", ros::Time(0), leftElbowTransform);
            listener.lookupTransform("/openni_depth_frame", "/left_shoulder_1", ros::Time(0), leftShoulderTransform);
            listener.lookupTransform("/openni_depth_frame", "/right_hand_1", ros::Time(0), rightHandTransform);
            listener.lookupTransform("/openni_depth_frame", "/right_elbow_1", ros::Time(0), rightElbowTransform);
            listener.lookupTransform("/openni_depth_frame", "/right_shoulder_1", ros::Time(0), rightShoulderTransform);
            listener.lookupTransform("/openni_depth_frame", "/torso_1", ros::Time(0), torsoTransform);
            listener.lookupTransform("/openni_depth_frame", "/neck_1", ros::Time(0), neckTransform);
        }
        catch (tf::TransformException ex) {
            ROS_ERROR("%s", ex.what());
            ros::Duration(1.0).sleep();
        }

        //Записываем координаты
        leftHand.x = -mRound(leftHandTransform.getOrigin().getX());
        leftHand.y = mRound(leftHandTransform.getOrigin().getY());
        leftHand.z = mRound(leftHandTransform.getOrigin().getZ());

        leftElbow.x = -mRound(leftElbowTransform.getOrigin().getX());
        leftElbow.y = mRound(leftElbowTransform.getOrigin().getY());
        leftElbow.z = mRound(leftElbowTransform.getOrigin().getZ());

        leftShoulder.x = -mRound(leftShoulderTransform.getOrigin().getX());
        leftShoulder.y = mRound(leftShoulderTransform.getOrigin().getY());
        leftShoulder.z = mRound(leftShoulderTransform.getOrigin().getZ());

        rightHand.x = -mRound(rightHandTransform.getOrigin().getX());
        rightHand.y = mRound(rightHandTransform.getOrigin().getY());
        rightHand.z = mRound(rightHandTransform.getOrigin().getZ());

        rightElbow.x = -mRound(rightElbowTransform.getOrigin().getX());
        rightElbow.y = mRound(rightElbowTransform.getOrigin().getY());
        rightElbow.z = mRound(rightElbowTransform.getOrigin().getZ());

        rightShoulder.x = -mRound(rightShoulderTransform.getOrigin().getX());
        rightShoulder.y = mRound(rightShoulderTransform.getOrigin().getY());
        rightShoulder.z = mRound(rightShoulderTransform.getOrigin().getZ());

        torso.x = -mRound(torsoTransform.getOrigin().getX());
        torso.y = mRound(torsoTransform.getOrigin().getY());
        torso.z = mRound(torsoTransform.getOrigin().getZ());

        neck.x = -mRound(neckTransform.getOrigin().getX());
        neck.y = mRound(neckTransform.getOrigin().getY());
        neck.z = mRound(neckTransform.getOrigin().getZ());

        ROS_INFO_STREAM("\nleftHand.x = " << leftHand.x <<
                        "\nleftHand.y = " << leftHand.y <<
                        "\nleftHand.z = " << leftHand.z <<
                        "\n " <<
                        "\nleftElbow.x = " << leftElbow.x <<
                        "\nleftElbow.y = " << leftElbow.y <<
                        "\nleftElbow.z = " << leftElbow.z <<
                        "\n " <<
                        "\nleftShoulder.x = " << leftShoulder.x <<
                        "\nleftShoulder.y = " << leftShoulder.y <<
                        "\nleftShoulder.z = " << leftShoulder.z <<
                        "\n " <<
                        //                        "\ntorso.x = " << torso.x <<
                        //                        "\ntorso.y = " << torso.y <<
                        //                        "\ntorso.z = " << torso.z <<
                        //                        "\n " <<
                        //                        "\nneck.x = " << neck.x <<
                        //                        "\nneck.y = " << neck.y <<
                        //                        "\nneck.z = " << neck.z <<
                        "\nleftElbow.x - leftShoulder.x = " << leftElbow.x - leftShoulder.x <<
                        "\nleftElbow.y - leftShoulder.y = " << leftElbow.y - leftShoulder.y <<
                        "\nleftElbow.z - leftShoulder.z = " << leftElbow.z - leftShoulder.z <<
                        "\nleftHand.x - leftElbow.x = " << leftHand.x - leftElbow.x <<
                        "\nleftHand.y - leftElbow.y = " << leftHand.y - leftElbow.y <<
                        "\nleftHand.z - leftElbow.z = " << leftHand.z - leftElbow.z <<
                        "\n ===================== \n");

        //Торс
        angle0 = (int)(TO_DEG(std::atan((rightShoulder.x - leftShoulder.x) / (rightShoulder.y - leftShoulder.y))));
        angle2 = (int)(TO_DEG(std::acos((neck.z - torso.z) /
                                        sqrt(std::pow(neck.x - torso.x, 2) + std::pow(neck.y - torso.y, 2) +
                                             std::pow(neck.z - torso.z, 2)))));

        if (angle2 < 0) {
            angle2 = 0;
        }

        //Преобразование координат левой руки
        coords temp;

        temp = leftHand;
        leftHand.x = mRound(temp.x * std::cos(TO_RAD(-angle0)));
        leftHand.y = mRound(temp.y * std::cos(TO_RAD(-angle0)) * std::cos(TO_RAD(-angle2)));
        leftHand.z = mRound(temp.z * std::cos(TO_RAD(-angle2)));

        temp = leftElbow;
        leftElbow.x = mRound(temp.x * std::cos(TO_RAD(-angle0)));
        leftElbow.y = mRound(temp.y * std::cos(TO_RAD(-angle0)) * std::cos(TO_RAD(-angle2)));
        leftElbow.z = mRound(temp.z * std::cos(TO_RAD(-angle2)));

        temp = leftShoulder;
        leftShoulder.x = mRound(temp.x * std::cos(TO_RAD(-angle0)));
        leftShoulder.y = mRound(temp.y * std::cos(TO_RAD(-angle0)) * std::cos(TO_RAD(-angle2)));
        leftShoulder.z = mRound(temp.z * std::cos(TO_RAD(-angle2)));

        //Левая рука
        //Получаем углы
        deltaX = leftElbow.x - leftShoulder.x;
        deltaY = leftElbow.y - leftShoulder.y;
        deltaZ = leftElbow.z - leftShoulder.z;

        prevAngle12 = angle12;
        prevAngle13 = angle13;

        angle13 = (int)(TO_DEG(std::atan(deltaZ / deltaX)));
        angle12 = (int)(TO_DEG(std::atan(deltaZ / deltaY)));

        //Решаем конфликты
        if (deltaX < 0) {
            angle13 = prevAngle13;
        }

        if (deltaY < 0) {
            angle12 = prevAngle12;
        }
        else {
            if (deltaZ > 0) {
                angle12 = -angle12;
            }

            if (angle12 > 0) {
                angle12 = 0;
            }
        }

//        //Локоть
//        temp;

//        leftHand.x = leftHand.x - leftShoulder.x;
//        leftHand.y = - (leftHand.y - leftShoulder.y);
//        leftHand.z = leftHand.z - leftShoulder.z;

//        leftElbow.x = leftElbow.x - leftShoulder.x;
//        leftElbow.y = - (leftElbow.y - leftShoulder.y);
//        leftElbow.z = leftElbow.z - leftShoulder.z;

//        leftShoulder.x = 0;
//        leftShoulder.y = 0;
//        leftShoulder.z = 0;

//        temp = leftHand;
//        leftHand.x = mRound(temp.x * std::cos(TO_RAD(angle13)));
//        leftHand.y = mRound(temp.y * std::cos(TO_RAD(angle12)));
//        leftHand.z = mRound(temp.z * std::cos(TO_RAD(angle13)) * std::cos(TO_RAD(angle12)));

//        temp = leftElbow;
//        leftElbow.x = mRound(temp.x * std::cos(TO_RAD(angle13)));
//        leftElbow.y = mRound(temp.y * std::cos(TO_RAD(angle12)));
//        leftElbow.z = mRound(temp.z * std::cos(TO_RAD(angle13)) * std::cos(TO_RAD(angle12)));


//        deltaX = leftHand.x - leftElbow.x;
//        deltaY = leftHand.y - leftElbow.y;
//        deltaZ = leftHand.z - leftElbow.z;

//        prevAngle11 = angle11;
//        angle11 = TO_DEG(std::atan(deltaX / deltaY));

//        if (deltaY < 0) angle11 = prevAngle11;


//        ROS_INFO_STREAM("\ndeltaX11 = " << deltaX <<
//                        "\ndeltaY11 = " << deltaY
//                        );

//        temp = leftHand;
//        leftHand.x = mRound(temp.x * std::cos(TO_RAD(angle11)) - temp.y * std::sin(TO_RAD(angle11)));
//        leftHand.y = mRound(temp.x * std::sin(TO_RAD(angle11)) + temp.y * std::cos(TO_RAD(angle11)));

//        temp = leftElbow;
//        leftElbow.x = mRound(temp.x * std::cos(TO_RAD(angle11)) - temp.y * std::sin(TO_RAD(angle11)));
//        leftElbow.y = mRound(temp.x * std::sin(TO_RAD(angle11)) + temp.y * std::cos(TO_RAD(angle11)));

//        deltaX = leftHand.x - leftElbow.x;
//        deltaY = leftHand.y - leftElbow.y;
//        deltaZ = leftHand.z - leftElbow.z;

//        angle10 = TO_DEG(std::atan(deltaZ / deltaX)) + 90;

//        //Решаем конфликты
//        if (deltaX < 0) angle10 = 0;


        ROS_INFO_STREAM("angle13 = " << angle13 <<
                        "\nangle12 = " << angle12 <<
                        //                        "\nangle11 = " << angle11 <<
                        //                        "\nangle10 = " << angle10 <<
                        "\nangle0 = " << angle0 <<
                        "\nangle2 = " << angle2 <<
                        "\n=================");

        //Очищаем сообщение
        servoControlMsg.servoData.clear();

        //Устанавливаем положения двигателей
        servoData.nServ = 0;
        servoData.time = 50;
        servoData.vel = 100;
        servoData.angle = angle0 + 7;
        servoControlMsg.servoData.push_back(servoData);

        servoData.nServ = 2;
        servoData.time = 50;
        servoData.vel = 100;
        servoData.angle = -angle2 + 42;
        servoControlMsg.servoData.push_back(servoData);

        servoData.nServ = 12;
        servoData.time = 10;
        servoData.vel = 200;
        servoData.angle = -angle12;
        servoControlMsg.servoData.push_back(servoData);

        servoData.nServ = 13;
        servoData.time = 10;
        servoData.vel = 200;
        servoData.angle = angle13;
        servoControlMsg.servoData.push_back(servoData);

//        servoData.nServ = 10;
//        servoData.time = 10;
//        servoData.vel = 200;
//        servoData.angle = -angle10;
//        servoControlMsg.servoData.push_back(servoData);

//        servoData.nServ = 11;
//        servoData.time = 10;
//        servoData.vel = 200;
//        servoData.angle = angle11;
//        servoControlMsg.servoData.push_back(servoData);

        //Посылаем в драйвер
        publisher.publish(servoControlMsg);

        rate.sleep();
    }

    return 0;
}
