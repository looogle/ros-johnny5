//Local includes
#include "johnny_skeleton_tracking.h"

//Main cycle
int main(int argc, char *argv[]) {
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;

//    ros::Publisher pub = nh.advertise<johnny5::robotState>(TOPIC_TRAKING_INFO, SIZE_MESSAGE_BUFFER);
//    ros::Subscriber sub = nh.subscribe(TOPIC_SERVO_CONTROL, SIZE_MESSAGE_BUFFER, &callbackFunc);

    ROS_INFO_STREAM("Skeleton tracking node started");

    while (ros::ok()) {
        ROS_INFO_STREAM("Skeleton tracking node in working...");
    }

    return 0;
}
