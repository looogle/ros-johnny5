#!/usr/bin/python3

import sys
import os
import subprocess
import time

import pprint

def getCommandOutput(command):
    return subprocess.getoutput(command)


def isStarted(process, args):
    command = ""
    condition = 0
    if args:
        """ 
            "pgrep -f" outout is not least than 1 not empty 
            pid ("pgrep -f" process itself), so condition = 2 
            will be good
        """
        command = "pgrep -f \"{0} {1}\"".format(process, args)
        condition = 2
    else:
        """
            "pgrep -x" outout is only one pid, but it may empty 
            or not, so condition = 1 and not empty value will be good
        """
        command = "pgrep -x \"{0}\"".format(process)
        condition = 1

    commandOutput = (getCommandOutput(command)).split("\n")

    if (len(commandOutput) >= condition) and (commandOutput[0] != ''):
        return True

    return False


def main():
    sensor = sys.argv

    isXtion = "false"
    if len(sys.argv) > 1:
        if str(sensor[1]) == "xtion":
            isXtion = "true"

    commands = [
            {"type":"launch", "pakage":"ppl_detection", "name":"ppl_detection.launch", "args": "xtion:={}".format(isXtion)},
            {"type":"node",   "pakage":"johnny5",       "name":"ssc32_driver",         "args": ""},
            {"type":"node",   "pakage":"johnny5",       "name":"johnny_life",          "args": ""}
            ]

    terminale = ["xterm", "-e"]

    for command in commands:
        name = command["name"]
        args = command["args"]
        pakage = command["pakage"]

        process = {"name":"","args":""}
        if command["type"] == "launch":
            """
                if "roslaunch" we'll use pakage + name as args of process 
                and roslaunch as process in function isStarted()
            """
            commandType = "roslaunch"
            process["name"] = commandType
            process["args"] = pakage + " " + name
        else:
            """
                if "rosrun" name will be enough as process name
                in function isStarted()
            """
            commandType = "rosrun"
            process["name"] = name

        commandStr = "{0} {1} {2}".format(commandType, pakage, name)

        if isStarted(process["name"], process["args"]):
            print("\nCommand \"{0}\" is already started.".format(commandStr))
        else:
            p = subprocess.Popen(terminale + [commandStr], 
                                 stdout=subprocess.PIPE, 
                                 universal_newlines=True
                                 )
            time.sleep(2)

            if isStarted(process["name"], process["args"]):
                print("\nCommand \"{0}\" was started in new terminale."
                      .format(commandStr + " " + args)
                      )
            else:
                print(("\nCommand \"{0}\" was not started for some reason."
                       "\nTry to start it manualy.")
                       .format(commandStr + " " + args)
                       )

    print("\nEnd of script.")


if __name__ == "__main__":
    main()